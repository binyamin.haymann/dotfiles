" VIM Configuration - Vincent Jousse
" Annule la compatibilite avec l’ancetre Vi : totalement indispensable
set nocompatible

" Activation de pathogen
call pathogen#infect()

" -- Affichage
set title                 " Met a jour le titre de votre fenetre ou de
" votre terminal
set number                " Affiche le numero des lignes
set ruler                 " Affiche la position actuelle du curseur
set wrap                  " Affiche les lignes trop longues sur plusieurs
" lignes
set scrolloff=3           " Affiche un minimum de 3 lignes autour du curseur
" (pour le scroll)
" -- Recherche
set ignorecase            " Ignore la casse lors d’une recherche
set smartcase             " Si une recherche contient une majuscule,
set incsearch
" re-active la sensibilite a la casse
set incsearch             " Surligne les resultats de recherche pendant la
" saisie
set hlsearch              " Surligne les resultats de recherche
" -- Beep
set visualbell            " Empeche Vim de beeper
set noerrorbells          " Empeche Vim de beeper
" Active le comportement ’habituel’ de la touche retour en arriere
set backspace=indent,eol,start
" Cache les fichiers lors de l’ouverture d’autres fichiers
set hidden
set mouse=a
set showcmd
set autowrite

source ~/binyamin-haymann/MyScripts/vim_whitespaces_script

syntax enable
filetype on
filetype plugin on
"filetype indent on

" Utilise la version sombre de Solarized
"let g:solarized_termcolors=256
"let g:solarized_termtrans=1
set background=dark
"colorscheme solarized

highlight ColorColumn ctermbg=black
set colorcolumn=81

"filetype plugin indent on
" show existing tab with 4 spaces width
set tabstop=4
" when indenting with '>', use 4 spaces width
set shiftwidth=4
" On pressing tab, insert 4 spaces
set expandtab
set autoindent
"set smartindent
"set cindent

autocmd FileType make setlocal noexpandtab

imap {<CR> {<CR>}<Left><CR><Up><Tab>
imap :<CR> :<CR><Tab>
"imap ( ()<Left>
"imap [ []<Left>
"imap /* /**/<Left><Left>
vnoremap (( <Esc>`>a)<Esc>`<i(<Esc>
vnoremap // <Esc>`>a*/<Esc>`<i/*<Esc>

imap `` <Esc>
map `` <Esc>
imap jj <Esc>

nmap <S-Enter> o<Esc>
"nmap <CR> o<Esc>

" Uncomment the following to have Vim jump to the last position when
" reopening a file
if has("autocmd")
    au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

"activate NeoComplete
let g:neocomplete#enable_at_startup=1

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
"let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

let g:syntastic_cpp_include_dirs=[ '/home/binyamin/binyamin-haymann/misc/', '/home/binyamin/binyamin-haymann/cpp/', '/home/binyamin/binyamin-haymann/Proj/', '/home/binyamin/git/syllabus/rd_syllabus/project/include/', '/home/binyamin/binyamin-haymann/projects/simple_shell/include/', '/home/binyamin/binyamin-haymann/projects/chsh/include/' ]

let g:syntastic_c_compiler_options="-ansi -pedantic-errors -Wall -Wextra -g"
let g:syntastic_cpp_compiler_options="-std=c++17 -pedantic-errors -Wall -Wextra -g"
let g:syntastic_python_python_exec="/usr/bin/python3"
